# BIT

**Codes for classes in [School of Computer Science & Technology](http://cs.bit.edu.cn/), [Beijing Institute of Technology](http://www.bit.edu.cn/)**

**Currently includes:**

- **2019**
  - Data Structure (数据结构)
  - Discrete Mathematics (离散数学)
  - Programming Method and Practice (程序设计方法与实践)
- **2020**
  - Combinatorial Mathematics (组合数学)
  - Computation Theory and Algorithm Analysis (计算原理与算法分析)
  - Object Oriented Technology and Methods (面向对象技术与方法)

***Continually update...***