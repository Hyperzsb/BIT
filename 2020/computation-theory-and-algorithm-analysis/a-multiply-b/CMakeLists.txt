cmake_minimum_required(VERSION 3.15)
project(a_multiply_b)

set(CMAKE_CXX_STANDARD 14)

add_executable(a_multiply_b main.cpp)